import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = 1;
        int counter = 1;
        try {
            System.out.println("Input an integer whose factorial will be computed");
            num = in.nextInt();

            // if num is 0, it will not be processed inside the if statements
            if (num > 0) {

                counter = num;

                // * For Loop Implementation
                for(int i = num-1; i > 0; i--) {
                    counter*= i;
                }

                // * While Loop Implementation
//                int i = num-1;
//                while (i > 0) {
//                    counter*= i;
//                    i--;
//                }


            } else if (num < 0) {
                System.out.println("Cannot compute for factorials of negative numbers...");
            }

        } catch (Exception e) {
            System.err.println("Invalid input. This program only accepts integer inputs.");
            e.printStackTrace();
        }

        // Do not show factorial result if num is negative
        if (num >= 0) {
            System.out.println("The factorial of " + num +" is "+ counter);
        }



    }
}